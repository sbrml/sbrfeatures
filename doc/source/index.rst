.. _sbrfeatures-doc:

Welcome to SBR features's documentation!
****************************************

.. _aim:

Aim
===
This package was developed within a research project aiming at monitoring small,
unstaffed wastewater treatment plants of the type sequencing batch reactor (SBR)
with unmaintained sensors. Right now, many unstaffed wastewater treatment plants
are not monitored at all. By using unmaintained sensor maintenance costs drop,
which means overcoming one of the main hurdles preventing a continuous
monitoring and therewith overcoming the bad reputation concerning the
performance of on-site wastewater treatment plants. The preprint of the related
publication `Beyond signal quality: The value of unmaintained pH, dissolved
oxygen and oxidation-reduction potential for remote performance monitoring of
on-site sequencing batch reactors <https://engrxiv.org/ndm7f/>`_ is available.

.. _content:

What does the package provide?
==============================
The *sbrfeatures* package is designed to automatically analyse an online pH or
dissolved oxygen signal from a cyclic reactor. Concretely, a valley in the pH
signal :func:`sbrfeatures.features_pH.aeration_valley` and a ramp in the
dissolved oxygen signal :func:`sbrfeatures.features_O2.aeration_ramp` feature
are identified, but any other feature could be added (see Contribution_ section
below).

.. figure:: methods_featuresdemo.png
   :scale: 80 %
   :alt: synthetic signal showing the two features valley and ramp
   :align: center

   Figure 1: Synthetic signal showing the two features valley and ramp.

Figure 1 is produced by the following code:

.. literalinclude:: ../../sbrfeatures/featuredemo_figure.py
  :language: python3
  :lines: 29-59, 75-123

The procedure to get these features is the following:

1. Smoothing the signal of one cycle. By default with the `scipy.signal.butter() <https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.butter.html>`_ function.

  .. literalinclude:: ../../sbrfeatures/basicfunctions.py
    :language: python3
    :lines: 53, 76-84
    :caption: :func:`smooth` from sbrfeatures.basicfunctions.py
    :name: smooth-py

2. Laying a spline on the smoothed signal and returning the spline representation using the `scipy.signal.splrep() <https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.splrep.html>`_ function.

  .. literalinclude:: /../../sbrfeatures/basicfunctions.py
    :language: python3
    :lines: 86, 108-110
    :caption: :func:`spline` from sbrfeatures.basicfunctions.py
    :name: spline-py

3. Evaluating the spline to find the extreme values. Thereby we assume that the spline is a good representation of our data and do not check automatically if this assumption is correct. To functions are used. Either the :func:`sbrfeatures.basicfunctions.signal_extrema` is used to find a minima or maxima in a smoothed signal. Or the :func:`sbrfeatures.basicfunctions.inflection_points` to find inflection points.

Potential application:

- Monitoring of batch-wise/cyclic processes with features that occur typically
    in healthy processes such as minima, maxima, and inflection points.
- Training algorithms for monitoring if the online measurement is paired with
    reference measurements.

.. _Repository:

Source code repository
======================

Stable release
--------------

The latest code for the stable release can be downloaded from here <https://gitlab.com/sbrml/sbrfeatures/-/archive/stable/sbrfeatures-stable.zip>

You can also clone the repository listed below and checkout the ``stable`` branch.

Development
-----------

To get the source code clone the git repository hosted at <https://www.gitlab.com/sbrml/sbrfeatures>

.. _Contribution:

Contribution
============
If you are interested in the development of this package or want to contribute
your own features please contact myschneider(at)meiru.ch.
The repository for the development of the package is `here
<https://gitlab.com/kakila/batchreactor-ml>`_.
You can also check `this <https://gitlab.com/kakila/sbrfeatures>`_ repository
to see how this package was used for the analysis reported in the publication
`Beyond signal quality: The value of unmaintained pH, dissolved oxygen and
oxidation-reduction potential for remote performance monitoring of on-site
sequencing batch reactors <https://engrxiv.org/ndm7f/>`_.

.. _API:

API
***

The features defined in our module accept a hierarchy. Basic features are the
general properties of signals, like extrema, inflection points, etc.
Sensor-type-specific features combine or select basic features to make them
specific for the signal analysed (e.g. valley in the pH signal during aeration
phase).

For most of the computations, we assume that the filtered signals have at least
their first derivative with respect to time continuous in the interval of the
analysed phase (i.e. aeration), i.e. they are  functions. However, some features
assume more regularity; for example, features that exploit spline
representations of the smoothed signals, assume that the smoothed signals (in
form of spline representations) have at least continuous derivatives with
respect to time up to order 2.

Basic functions
===============
.. automodule:: sbrfeatures.basicfunctions
   :members:
   :undoc-members:
   :show-inheritance:

Signal features
===============
The features defined in our module accept a hierarchy. Basic features are the
general properties of signals (extrema, inflection points). Sensor-type-
specific features (e.g. pH specific) combine or select basic features to make
them specific for the sensor signal analysed (e.g. valley in the pH signal
during aeration phase).
For most of the computations, we assume that the filtered signals have at least
their first derivative with respect to time continuous in the interval of the
analysed phase (aeration). However, some features assume more regularity;
for example, features that exploit spline representations of the smoothed
signals, assume that the smoothed signals (in form of spline representations)
have at least continuous derivatives with respect to time up to order 2.
Henceforth, we use the term derivative to mean derivative with respect to time.
(from Schneider, 2018)

pH features
-----------
.. automodule:: sbrfeatures.features_pH
  :members:
  :undoc-members:
  :show-inheritance:

O2 features
-----------
.. automodule:: sbrfeatures.features_O2
  :members:
  :undoc-members:
  :show-inheritance:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

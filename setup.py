# vim: set tabstop=4
# setup.py
#!/usr/bin/env python3
""" setup script for sbrfeatures module """

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>

from os import path

from setuptools import (
    setup,
    find_packages
)

from sbrfeatures import (
    __version__,
    __author__,
    __email__
)

this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name="sbrfeatures",
    packages=find_packages(),
    version=__version__,
    install_requires=["matplotlib>=2.1.1",
                      "numpy>=1.15.0",
                      "scipy>=1.0.0"],
    author=__author__,
    author_email=__email__,
    url="https://sbrml.gitlab.io/sbrfeatures",
    classifiers=["Development Status :: 3 - Alpha",
                 "Environment :: Other Environment",
                 "Intended Audience :: Science/Research",
                 "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
                 "Operating System :: OS Independent",
                 "Programming Language :: Python :: 3",
                 "Topic :: Scientific/Engineering",
                 "Topic :: Scientific/Engineering :: Information Analysis",
                 "Topic :: Scientific/Engineering :: Chemistry",
                 ],
    description="Feature extraction from Sequential Batch Reactors (SBR) signals",
    long_description=long_description,
    long_description_content_type='text/x-rst'
)

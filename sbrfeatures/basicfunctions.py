# vim: set tabstop=4
# basicfunctions.py
#!/usr/bin/env python3

""" Basic functions used to calculate features on sensor signals."""

# Copyright (C) 2018 Mariane Yvonne Schneider
# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>

############
## Imports
# built-ins
import warnings

# 3rd party
import numpy as np
from scipy.interpolate import (
    splrep,
    splev,
    sproot,
    splder,
    spalde,
)
from scipy.signal import (
    butter,
    filtfilt,
    argrelmax,
)
from scipy.optimize import brentq

############

## These are useful for rounding and comparisons
EPS = np.finfo(float).eps
LOGTOL = - int(np.round(np.log10(np.sqrt(EPS)))) # use x.round(decimals=LOGTOL)

def smooth(time, signal, *, smoother=None, **kwargs):
    """
    Smooth a signal.

    Arguments
    ---------
    time : array_like
        Time values.
    signal : array_like
        Signal values of the same length as t.

    Keyword arguments
    -----------------
    smoother : function, optional
        signal smoother. Takes the signal as the first positional argument.
        It returns the smoothed signal (array_like) and accept keyword
        arguments. If it is None, it uses :func:`scipy.signal.butter`.

    Returns
    -------
    array_like
        Smoothed signal, return type is the same as of the smoother function.
    """

    if smoother is None:
        dt = time[1] - time[0]
        fnyq = 1 / (2 * dt)
        def butter_(y, order=3, freq=fnyq/2):
            b, a = butter(order, freq/fnyq)
            return filtfilt(b, a, y)
        smoother = butter_

    return  smoother(signal, **kwargs)

def spline(t, s, *, degree=5, **kwargs):
    """Wrapper to :func:`scipy.interpolate.splrep`

    Arguments
    ---------
    t : array_like
        Time values.
    s : array_like
        Signal values of the same length as t.

    Keyword arguments
    -----------------
    degree : int, optional
        Degree of the spline, default is 5.

    Returns
    -------
    tuple
        A tuple (t, c, k) containing the vector of knots, the spline
        coefficients, and the degree of the spline.
    """

    spl = splrep(t, s, k=degree, **kwargs)

    return spl

def inflection_points(t, spl):
    """Return the coordinates of inflection points in the 3-tuple spl,
    representing a spline.

    At inflection points the 2nd derivative of the spline is zero and
    the lowest-order (above the 2nd) non-zero derivative is odd (3rd, 5th, etc).

    If the 1st derivative is zero the inflection point is marked as a saddle.

    Arguments
    ---------
    t : array_like
        Time values.
    spl : tuple
        A Scipy spline. See :func:`scipy.interpolate.splrep`.

    Returns
    -------
    array_like
        Times of the inflection points.
    array_like
        Values of the signal at the inflection points.
    bool, array_like
        A boolean array indicating if the inflection point is also a saddle
        point.

    See also
    --------
    spline: spline interpolation wrapping :func:`scipy.interpolate.splrep`
    """

    # Necessary condition for inflection d²s/dt² == 0, assuming that spl is a
    # good representation of the signal
    deriv2 = splder(spl, n=2)
    # minimum estimated number of roots (mest) is high to avoid warning
    # see sproot
    candidate_inflec = sproot(deriv2, mest=1000)

    x = None
    y = None
    is_saddle = None
    if candidate_inflec.size > 0:
        # All derivatives of the spline evaluated at all the identified
        # potential inflection points.
        dev_list = spalde(candidate_inflec, spl)
        # If there is more than 1 candidate spalde returns a list
        if not isinstance(dev_list, list):
            dev_list = [dev_list]

        # Sufficency by checking higher derivatives of odd order
        # First nonzero deriv > 2 is odd
        oddnonzero = lambda x: np.nonzero(~np.isclose(x[3:], 0))[0][0] % 2 == 0
        is_inflec = np.array([oddnonzero(x) for x in dev_list])
        # Checks if first derivative is close to 0
        is_stationary = np.array([np.isclose(x[1], 0) for x in dev_list])

        # Return values as 1d arrays
        x = candidate_inflec[is_inflec].ravel()
        is_saddle = (is_inflec & is_stationary).ravel()

        # take only the x values inside the time vector given
        ininterval = (x >= t.min()) & (x <= t.max())
        x = x[ininterval]
        is_saddle = is_saddle[ininterval]

        if x.size > 0:
            y = splev(x, spl)
        else:
            x = None
            y = None
            is_saddle = None

    return x, y, is_saddle

def signal_extrema(time, signal, *, bndvalue=None, bndwidth=None, der=0, extrema_type='max',
                   tol=None):
    """Find maxima or minima in a smooth signal.

    Arguments
    ---------
    time: array_like
        An array like time vector.
    signal: array_like
        Signal values at the given time vector or a tuple representing a
        spline object. If the values of the signal are given, a spline is
        constructed using the function :func:`spline`.

    Keyword arguments
    -----------------
    bndvalue: tuple of 2 floats or None, optional
        Minimum and maximum of the value of the signal. Minima/Maxima 
        lower/higher than these values are ignored. Default None.
    bndwidth: tuple of 2 floats or None, optional
        Minimum and maximum width of the extrema. Extrema with widths 
        smaller/larger than these bounds are ignored. The width is defined
        as the width of the local parabola at 90% of the extrema value.
        Default None.
    der: int, optional
        Order of the derivative in which to find the extrema. Default 0.
    extrema_type: str, optional
        Type of extrema to find 'min' to find minima and 'max' to find maxima.
        Default 'max'.
    tol: float
        Tolerance for the value of the signal. Maxima lower than this
        tolerance are ignored. Default sqrt(:py:data:`EPS`).

        .. deprecated:: 0.1.1
           Use `bndvalue` instead.

    Returns
    -------
    array_like
        Array with the indexes of the extrema
    array_like
        Array with the times of the extrema
    array_like
        Array with the signal values at the extrema
    dict
        `spline`: the spline used for the search.
        `widths`: the widths of the extrema

    See also
    --------
    spline: spline interpolation wrapping :func:`scipy.interpolate.splrep`
    
    Example
    -------
    >>> import numpy as np
    >>> t = np.linspace(0, 1, 100)
    >>> y = np.sin(2 * np.pi * 3 * t) * t
    >>> i, ti, yi, extr = signal_extrema(t, y)
    >>> print(ti);print(yi)
    [0.10762902 0.42328133 0.75372795]
    [0.09653839 0.41999544 0.7518678 ]


    Select only the middle maxima
    
    >>> i, ti, yi, extr = signal_extrema(t, y, bndvalue=(0.1, 0.5))
    >>> print(ti);print(yi)
    [0.42328133]
    [0.41999544]
    
    Force computation of widths

    >>> i, ti, yi, extr = signal_extrema(t, y, bndwidth=(None, None))
    >>> print(extr['widths'])
    [0.03892522 0.04672216 0.04721727]

    Select by widths

    >>> i, ti, yi, extr = signal_extrema(t, y, bndwidth=(4e-2, None))
    >>> print(ti);print(yi)
    [0.42328133 0.75372795]
    [0.41999544 0.7518678 ]

    """

    # If tol i used map it to new parameters
    if tol is not None:
        warnings.warn('tol is deprecated, use bndvalue instead', DeprecationWarning)
        if bndvalue is not None:
            raise ValueError('You cannot pass bndvalue and tol at the same time')
        bndvalue = (tol, None)

    # set default extrema_type to 'max'.
    sign = 1
    if extrema_type == 'min':
        sign = -1

    # check for tuple assuming correct representation of
    # scipy.interpolate.splrep.
    if isinstance(signal, tuple):
        spl = signal
    else:
        spl = spline(time, signal, degree=der+1+3)

    ds = [splder(spl, n=i) for i in range(der, der+3)] # i-st derivative

    try:
        # Compute exact roots for cp = critical points
        cp = sproot(ds[1], mest=time.size)     # critical points of der+1 derivative
    except ValueError:
        warnings.warn('signal_extrema: approximated root finding.',
                      RuntimeWarning)
        # Approximate the roots if none found above.
        ds_ = sign * splev(time, ds[0])
        d2s_ = sign * splev(time, ds[1])
        icp = np.nonzero((d2s_[0:-1] * d2s_[1:] < 0))[0]

        cp = np.array([brentq(
                        lambda x: sign * splev(x, ds[1]), time[i], time[i+1])
                        for i in icp])

    if cp.size > 0:
        # Filter critical points within time vector
        msk = (cp >= time.min()) & (cp <= time.max())

        d3s_ = sign * splev(cp, ds[2]) # der+2 derivative at critical points
        ds_ = splev(cp, ds[0]) # der derivative at critical points
        # Filter critical points min/max
        msk = msk & (d3s_ < 0)
        # Filter critical points by widths
        if bndwidth:
            mw, Mw = bndwidth
            #widths = 4 * np.sqrt(ds_ / np.abs(d3s_))
            theta=0.9
            widths = 2 * np.sqrt(2 * (1 - theta) * np.abs(ds_ / d3s_))
            if mw:
              msk = msk & (widths > mw)
            if Mw:
              msk = msk & (widths < Mw)
            widths = widths[msk]
        imax = np.nonzero(msk)[0]
        tM = cp[imax]

    else:
        # no local extrema found, take the maximum or minimum respectively,
        # likely to be at the boundaries
        ds_ = splev(time, ds[0])
        imax = np.argmax(sign * ds_)
        tM = time[imax]

    # Filter critical points by value
    msk = np.ones_like(imax) > 0
    if bndvalue:
        # use only those within bounds
        mv, Mv = bndvalue
        if mv:
          msk = msk & (ds_[imax] > mv) 
        if Mv:
          msk = msk & (ds_[imax] < Mv)

    extra = dict(spline=spl)
    tM = tM[msk]
    if tM.size == 0:
        imax = None
        tM = None
        dM = None
    else:
        imax = imax[msk]
        dM = ds_[imax]
        # map imax to original vector indexes
        imax = np.array([np.argmax(-np.abs(time-t0)) for t0 in tM])
        if 'widths' in locals():
            extra['widths'] = widths[msk]

    return imax, tM, dM, extra

if __name__ == '__main__':

    import matplotlib.pyplot as plt

    # helper functions
    def plot_inflec(t, s, spl, ti, yi, is_saddle):
        '''Plots inflection points.

        Arguments
        ---------
        t, s: array_like
            Datapoints defining the input curve.
        spl: tuple
            Tuple representing the spline.
        ti, yi, is_saddle: tuple
            Tuple with the result of the function :func:`inflection_points`.

        See also
        --------
        inflection_points: compute inflection points from spline
        '''
        plt.plot(t, splev(t, spl), '-', label='spline')
        if ti is not None:
            plt.plot(ti, yi, 'ok', label='inflections', markerfacecolor='none',
                     markersize=10)
            if is_saddle.any():
                plt.plot(ti[is_saddle], yi[is_saddle], '+k', label='saddles')
        plt.legend()
        plt.xlabel('t')
        plt.ylabel('X')

    def plot_extrema(t, s, tm, m, tM, M):
        plt.plot(t, s, '--', label='signal')
        if tm is not None:
            plt.plot(tm, m, 'go', label='local min')
        if tM is not None:
            plt.plot(tM, M, 'ro', label='local max')
        plt.xlabel('time')
        plt.ylabel('value')
        plt.legend()

    # signal_extrema example
    t = np.linspace(0, 1, 100)
    s = (np.tanh(30 * (t - 0.3)) + 1) * np.sin(2 * np.pi * 3 * t)
    _, tM, dM, extr =  signal_extrema(t, s)
    spl = extr['spline']
    M = splev(tM, extr['spline'])
    _, tm, dm, extr =  signal_extrema(t, s, extrema_type='min')
    m = splev(tm, extr['spline'])

    plt.figure()
    plot_extrema(t, s, tm, m, tM, M)
    plt.title('Local minima and maxima')
    plt.autoscale(enable=True, axis='x', tight=True)
    plt.show()

    # inflection_points examples
    t = np.linspace(-1, 1, 200)
    s = np.exp(-t**2/2/0.5**2) * t**3
    spl = spline(t, s)
    ti, yi, is_saddle = inflection_points(t, spl)

    plt.figure()
    plot_inflec(t, s, spl, ti, yi, is_saddle)
    plt.title('inflection_points(): Saddle and inflection point')
    plt.show()

    # difference between inflection_points and signal_extrema
    t = np.linspace(-1, 1, 200)
    s = t**4 + t
    spl = spline(t, s)
    ti, yi, is_saddle = inflection_points(t, spl)
    _, tM, dM, extr = signal_extrema(t, s, der=1, bndvalue=(2e-3,None))
    spl_se = extr['spline']
    M = splev(tM, spl_se)

    plt.figure()
    plot_inflec(t, s, spl_se, ti, yi, is_saddle)
    plot_extrema(t, s, None, [], tM, M)
    plt.title('signal_extrema()')
    plt.xlabel('t')
    plt.ylabel('X')
    plt.show()

    # signal_extrema implies inflection_points
    t = np.linspace(0, 1, 100)
    s = (np.tanh(30 * (t - 0.3)) + 1) * np.sin(2 * np.pi * 3 * t)
    spl = spline(t, s)
    ti, yi, is_saddle = inflection_points(t, spl)
    _, tM, dM, extr =  signal_extrema(t, s, der=1, bndvalue=(2e-3,None))
    M = splev(tM, extr['spline'])

    plt.figure()
    plot_inflec(t, s, spl, ti, yi, is_saddle)
    plot_extrema(t, s, None, [], tM, M)
    plt.title('signal_extrema() implies inflection_points()')
    plt.xlabel('t')
    plt.ylabel('X')
    plt.show()

    # plot spline
    t = np.linspace(0, 1, 100)
    noise = np.random.normal(0, 0.2, len(t))
    s = (np.tanh(30 * (t - 0.3)) + 1) * np.sin(2 * np.pi * 3 * t) + noise
    s_ = smooth(t, s)
    spl = spline(t, s_)
    M = splev(t, spl)

    plt.figure()
    plt.plot(t, M, '-', label='spline')
    plt.plot(t, s, '--', label='signal')
    plt.xlabel('time')
    plt.ylabel('value')
    plt.legend()
    plt.title('smooth() and spline()')
    plt.show()

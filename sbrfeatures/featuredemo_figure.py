# vim: set tabstop=4
# featuredemo_figure.py
#!/usr/bin/env python3

""" The script shows the implemented features on a synthetic signals.
    Run it as a module python -m sbrfeatures.featuredemo_figure without .py
"""

# Copyright (C) 2018 Juan Pablo Carbajal
# Copyright (C) 2018 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 20.02.2019

############
## Imports
# built-ins
import platform

# 3rd party
from functools import partial
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
try:
    import seaborn
    matplotlib.style.use('seaborn')
    seaborn.set_style('whitegrid')
except:
    pass

# user
from .features_pH import aeration_valley
from .features_O2 import aeration_ramp
from .basicfunctions import smooth
############

matplotlib.rcParams.update({'font.size': 12})
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
else:
    matplotlib.rcParams['font.family'] = 'sans-serif'
    matplotlib.rcParams['font.sans-serif'] = 'Arial'

def plot2dfeature(time, vals):
    """ Helpfunction to plot the features.

        Arguments
        ---------
        time : integer
            Time of the feature occurence (valey or ramp here).
        vals : float
            Values of signal at the point where the feature is identified.

        Returns
        -------
        tuple
            Contains to points where to plot.

    """
    point = plt.plot(time, vals[0], 'o')
    xlin = time + np.array([-0.05, 0.05])
    ylin = vals[0] + vals[1] * (xlin - time)
    line = plt.plot(xlin, ylin, '-')
    line[0].set_c(point[0].get_c())

    return point[0], line[0]

if __name__ == '__main__':
    # run as a module python -m sbrfeatures.featuredemo_figure without .py
    # Generate synthetic signal
    time = np.linspace(0, 1, 250)
    signal = np.exp(-time / 0.1) + (np.tanh((time - 0.3) / 0.1) + 1) / 2 \
                    + (np.tanh((time - 0.8) / 0.05) + 1) / 2 \
                    + 0.2 * np.sin( 2 * np.pi * 10 * time)
    # Addition of noise.
    sn = signal + 0.05 * np.random.randn(*signal.shape)

    # Smoothing function for all features
    nyqf = (1 / np.diff(time).min() / 2) # Nyquist freq.
    cut_freq = 0.02 * nyqf
    smoothfun = partial(smooth, order=3, freq=cut_freq)

    # pH: aeration valley
    t_valley, val_valley, ss = aeration_valley(time, sn, smoother=smoothfun)
    # O2: aeartion ramp
    t_ramp, val_ramp, _ = aeration_ramp(time, sn, smoother=smoothfun)

    plt.figure()
    plt.plot(time, sn, '.k', label='Signal')
    plt.plot(time, ss, label='Smoothed')

    vh = plt.plot(t_valley, val_valley, 'o')
    rh, _ = plot2dfeature(t_ramp, val_ramp)

    # Annotations
    valley_color = vh[0].get_c()
    bbox_props = dict(boxstyle="rarrow, pad=0.3", \
                      fc=valley_color, ec=valley_color, lw=2, alpha=0.8)
    th = plt.text(t_valley-0.02, val_valley-0.1, "Valley", ha="right", \
                  va="top", rotation=45, size=12, bbox=bbox_props)
    bbox_props['fc'] = bbox_props['ec'] = rh.get_c()
    th = plt.text(t_ramp-0.02, val_ramp[0]+0.1, "Ramp", ha="right", \
                  va="bottom", rotation=-45, size=12, bbox=bbox_props)

    plt.legend(frameon=True)
    plt.xlabel('time [arbitrary unit]')
    plt.ylabel('signal [arbitrary unit]')
    plt.show()
